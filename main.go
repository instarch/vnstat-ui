package main

import (
	"flag"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
)

var (
	VERSION = "0.8.2"
	BUILD   = "dev"

	// GRAPH_TYPES is a mapping of friendly graph types to command line flags
	GRAPH_TYPES = map[string]string{
		"summary": "vs",
		"hourly":  "h",
		"daily":   "d",
		"monthly": "m",
		"top":     "t",
	}

	addr    = flag.String("b", "0.0.0.0", "address to use when listening for requests")
	port    = flag.Int("p", 7000, "port to use when accepting new requests")
	dataDir = flag.String("d", "/var/lib/vnstat", "data directory for vnstat")
	graphs  = flag.String("g", "summary,hourly,daily,monthly,top", "comma-separated list of graphs to display")
	ifaces  = flag.String("i", "", "comma-separated list of interfaces to display graphs for")

	// interfaces holds the list of interface names that we'll produce graphs for
	interfaces []string

	// graphTypes holds the list of graph types that we'll return for each interface
	graphTypes []string

	// indexTpl is the parsed HTML template for the graph page
	indexTpl *template.Template

	// licenseTpl is the parsed HTML template for the license page
	licenseTpl *template.Template
)

func init() {
	var (
		files []os.FileInfo
		err   error
	)

	log.Printf("starting vnstat-ui %s (%s)", VERSION, BUILD)
	flag.Parse()

	// parse templates from go-bindata
	indexTpl = parseTemplateAsset("index")
	licenseTpl = parseTemplateAsset("license")

	if *ifaces == "" {
		// display graphs for all interfaces that vnstat monitors
		if files, err = ioutil.ReadDir(*dataDir); err != nil {
			log.Fatalf("unable to determine monitored interfaces: %s", err)
		}

		// all files that do not start with a period are considered monitored by vnstat
		for _, file := range files {
			if !file.IsDir() && file.Name()[0] != '.' {
				interfaces = append(interfaces, string(file.Name()))
			}
		}
	} else {
		// split the list of specific interface names
		interfaces = strings.Split(*ifaces, ",")
	}
	sort.Strings(interfaces)

	// determine which graph types to produce
	if *graphs == "" {
		for key := range GRAPH_TYPES {
			graphTypes = append(graphTypes, key)
		}
	} else {
		for _, key := range strings.Split(*graphs, ",") {
			if isValidGraphType(key) {
				graphTypes = append(graphTypes, key)
			}
		}
	}
}

func main() {
	router := httprouter.New()

	router.GET("/", index)
	router.GET("/iface/:interface", onlyIface)
	router.GET("/graph/:interface/:graph", makeGraph)
	router.GET("/license", showLicense)

	bind := fmt.Sprintf("%s:%d", *addr, *port)
	log.Printf("accepting requests on %s", bind)
	log.Fatal(http.ListenAndServe(bind, router))
}

// index displays the configured graphs for all configured interfaces.
func index(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	title := "Traffic Summary"
	showGraphs(w, title, interfaces)
}

// onlyIface displays the configured graphs for a single interface that is specified in the URI.
func onlyIface(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	iface := ps.ByName("interface")
	title := fmt.Sprintf("%s Traffic Summary", iface)

	showGraphs(w, title, []string{iface})
}

// showGraphs is the shared logic between the page for all interaces and a single interface.
func showGraphs(w http.ResponseWriter, title string, ifaces []string) {
	// data to pass to the template
	data := map[string]interface{}{
		"Title":      title,
		"Graphs":     graphTypes,
		"Interfaces": ifaces,
		"Version":    VERSION,
	}

	// render the template
	if err := indexTpl.Execute(w, data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		reportError(w, err, "template error")
	}
}

// makeGraph handles the graph creation on demand.
func makeGraph(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	var (
		flag string
		img  []byte
		err  error
	)

	// grab the appropriate parameters from the URI
	iface := ps.ByName("interface")
	graphType := ps.ByName("graph")

	// do a little validation
	{
		if !isValidIface(iface) {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, `Invalid interface "%s"`, iface)
			return
		}

		if !isValidConfiguredGraphType(graphType) {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, `Invalid graph type "%s"`, graphType)
			return
		}
	}

	flag = GRAPH_TYPES[graphType]

	// request the image for the specified interface and graph type
	cmd := exec.Command("vnstati", "-i", iface, "-o", "-", fmt.Sprintf("-%s", flag))
	if img, err = cmd.Output(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		reportError(w, err, "graph generation error")
		return
	}

	// some basic caching
	expires := time.Now().Add(5 * time.Minute)
	w.Header().Set("Expires", expires.Format(time.RFC1123))
	w.Header().Set("Content-Type", "image/png")

	// send the graph to the browser
	if _, err := w.Write(img); err != nil {
		reportError(w, err, "failed to send graph")
		return
	}

	w.Write(img)
}

// showLicense displays the project's license
func showLicense(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	if err := licenseTpl.Execute(w, time.Now().Year()); err != nil {
		reportError(w, err, "template error")
	}
}

// isValidIface determines whether name is in the list of interfaces that vnstat-ui is configured to display graphs
// for.
func isValidIface(name string) bool {
	for _, iface := range interfaces {
		if iface == name {
			return true
		}
	}

	return false
}

// isValidGraphType determines whether name is a valid graph type that vnstat-ui can produce.
func isValidGraphType(name string) bool {
	for gtype := range GRAPH_TYPES {
		if gtype == name {
			return true
		}
	}

	return false
}

// isValidConfiguredGraphType determines whether name is a valid graph type that vnstat-ui is configured to produce.
// This is distinct from isValidGraphType because vnstat-ui may be configured to only produce summary graphs (for
// example), and requesting a daily graph should result in an error.
func isValidConfiguredGraphType(name string) bool {
	for _, gtype := range graphTypes {
		if gtype == name {
			return true
		}
	}

	return false
}

// reportError is an abstraction for rendering errors to stdout and to the web browser.
func reportError(w io.Writer, err error, msg string) {
	if err != nil {
		msg = fmt.Sprintf("%s: %s", msg, err)
		fmt.Fprintf(w, msg)
		log.Print(msg)
	}
}

// parseTemplateAsset converts a bundled asset (from go-bindata) into a usable template for rendering content.
func parseTemplateAsset(name string) *template.Template {
	var (
		data []byte
		err  error
	)

	if data, err = Asset(fmt.Sprintf("templates/%s.html", name)); err != nil {
		log.Fatalf(`unable to parse template "%s": %s`, name, err)
	}

	t := template.New(name)
	t.Parse(string(data))

	return t
}
