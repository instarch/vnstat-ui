VERSION ?= $(shell git describe --long | cut -d - -f 1 | sed 's/[[:alpha:]]//g')
BUILD ?= $(shell git describe --long | cut -d - -f 3)

build: assets
	go build -ldflags '-s -X main.VERSION=$(VERSION) -X main.BUILD=$(BUILD)' -o vnstatui

assets:
	go-bindata -ignore=\.swp templates/...

deps:
	go mod download

clean:
	rm -rf vendor/ vnstatui
