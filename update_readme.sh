#!/bin/bash
#
# Pre-commit hook to update the README based on what is in the PKGBUILD
#

pkgver=$(awk '/\s+VERSION =/ {print $NF}' main.go | sed 's/"//g')

sed -i "s,^:Version: .*,:Version: ${pkgver},;s,^:Date: .*,:Date:   $(date +%Y-%m-%d)," README.rst

git add README.rst
